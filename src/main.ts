import { createApp } from 'vue'
import App from './App.vue'
import { setupAuth0, auth0Plugin } from "./auth0";
import { domain, client_id } from "../auth_config";
import './registerServiceWorker'
import "@/assets/css/main.css"


setupAuth0({ domain, client_id }).then(async () => {
  createApp(App)
    .use(auth0Plugin)
    .mount("#app");
});
