export type Message = {
    id: string;
    content: string;
    createdAt: string;
    user: User;
}

export type User = {
    id?: string;
    lastName?: string,
    firstName?: string,
    email: string;
    role?: string;
    imgUrl?: string;
    createdAt: string;
}

export type State = {
    token: string;
    user?: User;
    messages: Message[],
    participants: User[];
};

export const state: State = {
    token: "",
    user: undefined,
    messages: [],
    participants: []
};