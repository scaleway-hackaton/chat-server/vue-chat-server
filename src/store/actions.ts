import { store } from "@/store";
import { fetcher, fetchAsync } from "@/api/fetchers";
import { queries } from "@/api/queries";
import { mutations } from "@/api/mutations";
import { $auth0 } from "@/auth0";
import { Auth0Client } from "@auth0/auth0-spa-js";
import { Message, User } from "./state";

export const actionsList = {
  getCurrentUser: async (): Promise<User> => {
    const { token } = store.state;
    if (!token) {
      Promise.reject("No token provided");
    }
    const result = await fetchAsync(token, fetcher, queries.getUser, {
      variables: { email: $auth0.state.user.email },
    });
    if (result.errors) {
      //TODO: Gerer error
      console.error(result.errors);
    }
    return result.data.user[0];
  },
  createUserByToken: async () => {
    const { token } = store.state;
    const userToken = $auth0.state.user;
    const variables = {
      email: userToken.email,
      firstName: userToken.name,
      lastName: userToken.nickname,
      imgUrl: userToken.picture,
    };
    const result = await fetchAsync(token, fetcher, mutations.createUser, {
      variables,
    });
  },
  getMessagesPaginated: async (): Promise<Message[]> => {
    const { token } = store.state;
    const result = await fetchAsync(
      token,
      fetcher,
      queries.getMessagesPaginated
    );
    store.mutations.setMessages(result?.data?.message);
    return result?.data?.messages as Message[];
  },
  createMessage: async (content: string, userId: string) => {
    const { token } = store.state;
    const result = await fetchAsync(token, fetcher, mutations.createMessage, {
      variables: { content, userId },
    });
    if (result.errors) {
      return Promise.reject();
    }
    return Promise.resolve();
  },
  getAllUser: async () => {
    const { token } = store.state;
    const result = await fetchAsync(token, fetcher, queries.getAllUser);
    store.mutations.setParticipants(result?.data?.user);
  },
};
