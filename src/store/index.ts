import { reactive } from "vue";
import { state } from "./state";
import { mutationsList as mutations } from "./mutations";
import { actionsList as actions } from "./actions";

export const store = reactive({
  state,
  mutations,
  actions,
});