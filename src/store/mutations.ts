import { store } from "@/store";
import { Message, User } from "./state";

export const mutationsList = {
  setToken: (token: string) => {
    store.state.token = token;
  },
  setMessages: (messages: Message[]) => {
    messages?.forEach((message) => {
      if (!store.state.messages.find((item) => item.id === message.id)) {
        store.state.messages.push(message);
      }
    });
  },
  setUser: (user: User) => {
    store.state.user = user;
  },
  setParticipants: (participants: User[]) => {
    store.state.participants = participants
  },
};
