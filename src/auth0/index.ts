import { reactive } from 'vue'
import createAuth0Client from '@auth0/auth0-spa-js'

export const $auth0 : any = reactive({
  client: null,
  state: {
    isLoading: true,
    isAuthenticated: false,
    error: null,
    user: null,
    token: null,
  }
})

type optionAuth0 = {
  domain: string;
  client_id: string;
}

export async function setupAuth0(option: optionAuth0) {
  $auth0.client = await createAuth0Client({
    ...option,
    redirect_uri: window.location.origin,
    cacheLocation: 'localstorage'
    // audience: options.audience
  })
  const search = window.location.search
  const comingFromRedirect = search.includes('code=') && search.includes('state=')
  try {
    // If the user is returning to the app after auth success with Auth0.
    if (comingFromRedirect) {
      // handle the redirect and retrieve tokens
      const { appState } = await $auth0.client.handleRedirectCallback()
    }
  } catch (e) {
    $auth0.state.error = e
  } finally {
    // Initialize authentication state
    $auth0.state.isAuthenticated = await $auth0.client.isAuthenticated()
    $auth0.state.user = await $auth0.client.getUser()
    $auth0.state.isLoading = false
  }
}


export function auth0Plugin(app: any) {
  app.use(Auth0Plugin)
}

/**
 * A Vue plugin to expose the wrapper object throughout the application.
 * The Auth0 client is available as $auth0.client.
 */
export const Auth0Plugin = {
  install(app : any, options : any) {
    app.config.globalProperties.$auth0 = $auth0
  }
}