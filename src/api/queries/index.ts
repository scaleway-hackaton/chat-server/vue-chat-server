import { getUser } from "./user/getUser";
import { getMessagesPaginated } from "./message/getMessagesPaginated";
import { getAllUser } from './user/getAllUser'

export const queries = {
  getUser,
  getMessagesPaginated,
  getAllUser,
};