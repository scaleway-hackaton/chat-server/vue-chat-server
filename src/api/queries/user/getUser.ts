export const getUser = `query getUser($email: String) {
  user(where: {email: {_eq: $email}}) {
    id
    email
    firstName
    lastName
    role
    createdAt
    imgUrl
  }
}`
