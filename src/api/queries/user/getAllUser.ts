export const getAllUser = `query getAllUser {
  user {
    id
    email
    firstName
    lastName
    role
    createdAt
    imgUrl
  }
}`
