export const getMessagesPaginated = `query getMessagesPaginated {
    message {
      id
      content
      createdAt
      user {
        email
        firstName
        imgUrl
        lastName
        role
      }
    }
  }
  `