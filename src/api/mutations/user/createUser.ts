export const createUser = `mutation createUser($email: String!, $firstName: String!, $imgUrl: String, $lastName: String!) {
  insert_user(objects: {email: $email, firstName: $firstName, imgUrl: $imgUrl, lastName: $lastName}) {
    affected_rows
  }
}`
