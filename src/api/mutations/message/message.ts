export const createMessage = `mutation insertMessage($content: String!, $userId: uuid!) {
    insert_message_one(object: {content: $content, userId: $userId}) {
      content
    }
  }
  `