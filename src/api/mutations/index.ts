import { createUser } from "./user/createUser";
import { createMessage } from "./message/message";

export const mutations = {
  createUser,
  createMessage,
};
