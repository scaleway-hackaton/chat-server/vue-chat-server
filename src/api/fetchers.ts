const API_URL = "https://hasura.k8s-fun.lperdereau.fr/v1/graphql";

export const fetcher = (token: string, query: string, options?: {headers?: any, variables?: any}) =>
  fetch(API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
      ...options?.headers,
    },
    body: JSON.stringify({
      query,
      variables:options?.variables,
    }),
  });

export const fetchAsync = async (
  token: string,
  fetcher: any,
  query: string,
  headers?: any,
  variables?: any
) => {
  const response = await fetcher(token, query, headers, variables);
  if (!response.ok) {
    throw response;
  }
  try {
    return response.json();
  } catch (err) {
    console.error("Error parsing JSON", err);
  }
};